<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "libros".
 *
 * @property int $id
 * @property string|null $titulo
 * @property int|null $autor
 * @property int|null $año
 * @property string|null $idioma
 * @property string|null $editorial
 * @property int|null $isbn
 */
class Libros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'libros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['autor', 'año', 'isbn'], 'integer'],
            [['titulo', 'editorial'], 'string', 'max' => 50],
            [['portada'], 'string', 'max' => 255],
            [['idioma'], 'string', 'max' => 20],
            [['autor'], 'exist', 'skipOnError' => true, 'targetClass' => Autores::className(), 'targetAttribute' => ['autor' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'autor' => 'Autor',
            'año' => 'Año',
            'idioma' => 'Idioma',
            'editorial' => 'Editorial',
            'isbn' => 'Isbn',
            'portada' => 'Portada',
        ];
    }

    /**
     * Gets query for [[Autor0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAutor0()
    {
        return $this->hasOne(Autores::className(), ['id' => 'autor']);
    }
    
}
