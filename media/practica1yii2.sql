-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-03-2020 a las 13:31:44
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `practica1yii2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `autores`
--

CREATE TABLE `autores` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `biografia` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `autores`
--

INSERT INTO `autores` (`id`, `nombre`, `biografia`, `foto`) VALUES
(1, 'Charles Dickens', 'Portsmouth, (1812-1870). Periodista, dramaturgo y novelista, conoció desde niño las duras condiciones de vida de las clases humildes, debido a las deudas contraidas por su padre. A la denuncia de estas condiciones dedicó gran parte de su obra.', 'dickens1.jpg'),
(2, 'Emily Brönte', '(Yorkshire, 1818-1848) Al morir la madre, las hijas fueron enviadas a un internado de pésimas características, donde enfermaron de tuberculosis. Cscribía hermosos poemas, descubiertos por su hermana Charlotte. Tres hermanas, Charlotte, Anne y Emily.', 'bronte1.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entradas`
--

CREATE TABLE `entradas` (
  `id` int(11) NOT NULL,
  `texto` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `entradas`
--

INSERT INTO `entradas` (`id`, `texto`) VALUES
(1, 'Bienvenido a mi app. Elige una opcion del menu superior'),
(2, '¿Qué tal? Tienes nuestro menu en la barra superior'),
(3, '¡Hola! ¿Qué opción prefiere del menu superior?'),
(4, '¿Cómo va todo? ¿En qué podemos ayudarte? Echa un vistazo al menu superior');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libros`
--

CREATE TABLE `libros` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `autor` int(11) DEFAULT NULL,
  `año` int(11) DEFAULT NULL,
  `idioma` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `editorial` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `isbn` int(11) DEFAULT NULL,
  `portada` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `libros`
--

INSERT INTO `libros` (`id`, `titulo`, `autor`, `año`, `idioma`, `editorial`, `isbn`, `portada`) VALUES
(1, 'Cuento de Navidad', 1, 1843, 'castellano', 'Bambu Editorial', 2147483647, 'navidad1.jpg'),
(2, 'Oliver Twist', 1, 1839, 'euskera', 'Vicens Vives', 2147483647, 'oliver1.jpg'),
(3, 'Cumbres Borrascosas', 2, 1843, 'castellano', 'Alianza Editorial', 2147483647, 'cumbres1.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `autores`
--
ALTER TABLE `autores`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `entradas`
--
ALTER TABLE `entradas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `libros`
--
ALTER TABLE `libros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKlibrosautores` (`autor`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `autores`
--
ALTER TABLE `autores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `entradas`
--
ALTER TABLE `entradas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `libros`
--
ALTER TABLE `libros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `libros`
--
ALTER TABLE `libros`
  ADD CONSTRAINT `FKlibrosautores` FOREIGN KEY (`autor`) REFERENCES `autores` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
