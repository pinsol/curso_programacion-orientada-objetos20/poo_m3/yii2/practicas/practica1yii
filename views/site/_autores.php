
  <div  class="col-sm-5 col-md-5">
    <div class="thumbnail">
      <div class="caption altura1"> <?= \yii\helpers\Html::img("@web/image/" . $model->foto,[
            'alt'=>"alternativo",
            'class'=>'foto1'
        ]) ?>
          
        <h3><?= $model->nombre ?></h3>
        <p><?= $model->biografia ?></p>
      </div>
    </div>
  </div>
