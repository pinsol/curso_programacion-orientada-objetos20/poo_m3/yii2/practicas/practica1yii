<div  class="col-sm-5 col-md-5">
    <div class="thumbnail">
      <div class="caption altura1"> <?= \yii\helpers\Html::img("@web/image/" . $model->portada,[
            'alt'=>"alternativo",
            'class'=>'foto1'
        ]) ?>
          
        
        <h3><?= $model->titulo ?></h3>
        <p>de <?= $model->autor0->nombre ?></p>
        <p>Año: <?= $model->año ?></p>
        <p>Idioma: <?= $model->idioma ?></p>
        <p>Editorial: <?= $model->editorial ?></p>
        <p>ISBN: <?= $model->isbn ?></p>
      </div>
    </div>
  </div>