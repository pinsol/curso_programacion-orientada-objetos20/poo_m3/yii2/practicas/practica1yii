<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Libros;
use app\models\Autores;
use app\models\Entradas;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     
    public function actionIndex()
    {
        return $this->render('index');
    }
    */
    public function actionIndex()
    {
        $numReg = Entradas::find()->count();
        $random = random_int(0, $numReg - 1);
        $texto = Entradas::find()->offset($random)->one();
        
        return $this->render('index',[
            'texto' => $texto,
        ]);
    }
    
    public function actionLibros()
    {
        $query=Libros::find();
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 3,
            ],
        ]);
        
        return $this->render('libros',[
            'data'=>$dataProvider,
        ]);
    }
    
    public function actionAutores()
    {
        $query=Autores::find();
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 3,
            ],
        ]);
        
        return $this->render('autores',[
            'data'=>$dataProvider,
        ]);
    }
}
